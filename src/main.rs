// zero2prod is the library name
use sqlx::PgPool;
use std::env;
use zero2prod::configuration::get_configuration;
use zero2prod::startup::run;

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let configuration = get_configuration().expect("Failed to read configuration.");

    // Read env variable telling us whether we're on dev or ci.
    let env = match env::var("APP_ENV")
        .expect("Missing APP_ENV environment variable")
        .as_str()
    {
        "ci" => zero2prod::configuration::Environment::CI,
        _ => zero2prod::configuration::Environment::Dev,
    };

    let connection_pool = PgPool::connect(&configuration.database.connection_string(env))
        .await
        .expect("Failed to connect to Postgres.");

    // We have removed the hard-coded `8000` - it's now coming from our settings!
    let address = format!("127.0.0.1:{}", configuration.application_port);
    let listener = std::net::TcpListener::bind(address).expect("Failed to bind random port");

    println!(
        "Application running on port {}",
        listener.local_addr().unwrap().port()
    );

    run(listener, connection_pool)?.await
}
