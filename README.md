# Database
- To start Docker Postgres database and run migrations:
```shell
SKIP_DOCKER=true ./scripts/init_db.sh
```
- To run queries inside the Docker Postgres database container:
```shell
docker exec -it <container_name> psql -U <username> -d <database_name>
```
